from django.db import models
from datetime import datetime

# Create your models here.

class Author(models.Model):
    Name = models.CharField(verbose_name="Имя", max_length=100)
    age = models.PositiveSmallIntegerField("Возраст", default=0)
    image = models.ImageField('Изображение', upload_to='Author/',null=True, blank=True)
    w = models.BooleanField(verbose_name='Женщина', default=False)
    m = models.BooleanField(verbose_name='Мужчина',default=False)
    def __str__(self):
        return self.Name


class Record(models.Model):
    title = models.CharField('Название записи', max_length=100)
    Author = models.ForeignKey("Author", on_delete=models.SET_NULL, null=True)
    descrition = models.TextField("Описание", max_length=100, null=True, blank=True)
    date = models.DateTimeField('Дата создания записи', default=datetime.now, blank=True)
    def __str__(self):
        return self.title


class Tags(models.Model):
    tag_title = models.CharField("Название тега", max_length=200)
    record = models.ForeignKey("Record", on_delete=models.SET_NULL, null=True)
    descrition_tags = models.TextField("Описание тега", max_length=100, null=True, blank=True)
    def __str__(self):
        return self.tag_title